#######################################################
# Exercise-1:
# Fill in the following methods:
# Good Luck!

# 1
def biggest(a,b,c)
  # Return the maximum number among a, b and child
end

# 2
def longest(a,b,c)
  # Return an array with the string as first item, and length as second item
end

# 3
def age_category(birth_year)
  # Return the "child" if age is smaller 18, "adult" if age is between 18 and 64, and "senior" if age is 65 or more
  # You can assume the current year is 2013 (or research a way to retrieve it)
  # if birth_year is in the future - returns nil
end

# 4
def full_name(first_name, middle_name = nil, last_name = nil)
  # Return the capitalized full name, according to the names of a person
  # Each of the names might be nil
end

# 5
def find(arr, value)
  # find if a value (string or symbol) is inside an array (of strings and symbols).
  # The method is indifferent to symbols and strings
end

###################################################################
# The following methods test the above methods.
# Don't change them:
def test_biggest
  biggest(2, 3, 4)  == 4 &&
  biggest(4, 3, 2)  == 4 &&
  biggest(2, 4, 3)  == 4
end

def test_longest
  longest("my" , "name" , "is") == ["name", 4] &&
  longest("name" , "is", "my")  == ["name", 4] &&
  longest("is" , "my", "name")  == ["name", 4]
end

def test_age_category
  age_category(2000) == "child"  &&
  age_category(1925) == "senior" &&
  age_category(2022) == nil      &&
  age_category(1982) == "adult"
end


def test_full_name
  full_name("george", "w.", "bush") == "George W. Bush" &&
  full_name("bill", nil, "clinton") == "Bill Clinton" &&
  full_name("bill", "clinton")      == "Bill Clinton" &&
  full_name("madonna") == "Madonna"
end

def test_find
  find([:admin, :operator], 'operator') == true &&
  find(['admin', 'operator'], :operator) == true
end

##
# run tests:
%w(biggest longest age_category full_name find).each do |name|
  puts "Exercise #{name} is " + (send("test_#{name}") ? "correct" : "incorrect")
end
